import { ScrollView, StyleSheet, Text, View } from 'react-native'
import React, { useEffect } from 'react'
import { useSelector } from 'react-redux';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { router } from 'expo-router';
import { RootState } from '../../../types/reduxState/ReduxState';
import { themeGlobal } from '../../../styles/themeGlobal';
import CardPromo from '../../../components/exam/CardPromo';

const promotions = () => {
  const products = useSelector((state: RootState) => state.product.products);
  useEffect(() => {
    const isAuth = async () => {
      try {
        const userData = await AsyncStorage.getItem("userData");
        const parsedUserData = userData ? JSON.parse(userData) : null;

        if (!parsedUserData) {
          router.replace('/login');
        }
      } catch (error) {
        console.error("Error checking user authentication:", error);
      }
    };

    isAuth();
  }, []); 
  return (
    <View style={styles.container}>
      <ScrollView>
      <View style={styles.titleSectionWrapper}>
          <Text style={themeGlobal.themeTextGlobal.h5}> Room ideas</Text>
        </View>
        <ScrollView  style={styles.fullImageCardsWrapper}>
          {products.map &&
            products.map((product, index: number) => (
              <View style={styles.fullImageCardWrapper} key={index}>
                <CardPromo
                  title={product.title ?? ""}
                  image={product.image ?? ""}
                  price={product.price ?? 0} onClicklien={function (): void {
                    throw new Error('Function not implemented.');
                  } }                 />
              </View>
            ))}
        </ScrollView>

      </ScrollView>

    </View>
  )
}
  

export default promotions

const styles = StyleSheet.create({
  container: {
    backgroundColor: "white",
    flex: 1,
  },
  carouselWrapper: {
    marginBottom: 20,
    width: "100%",
    height: 188,
    paddingHorizontal: 24,
  },
  titleSectionWrapper: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    paddingHorizontal: 24,
  },
  fullImageCardsWrapper: {
    marginBottom: 20,
  },
  fullImageCardWrapper: {
    marginLeft: 20,
    borderRadius: 10,
    overflow: "hidden",
  },
  productCardsWrapper: {
    marginBottom: 20,
    paddingHorizontal: 20,
    flexDirection: "row",
    justifyContent: "space-around",
    flexWrap: "wrap",
    overflow: "hidden",
  },
  columnWrapper: {
    gap: 20,
  },
  productCardWrapper: {
    marginBottom: 20,
    borderRadius: 10,
    overflow: "hidden",
  },
})