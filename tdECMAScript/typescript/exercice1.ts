function addition(a: number, b: number): number {
    return a + b;
}
let resultat = addition(10, 5); // TypeScript signale une erreur ici
console.log(resultat);
